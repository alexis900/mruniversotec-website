<!doctype html>
<html lang="es">
<head>
		<!--Meta-->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<!--Title-->
		<title>MrUniversoTec</title>
		<!--Favicon-->
		<link rel="icon" type="image/png" href="/images/mifavicon.png" />
		<!--Style-->
		<link rel="stylesheet" href="/css/header.css" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="/css/icon.css" media="screen" title="no title" charset="utf-8">
		<link rel="stylesheet" href="/css/footer.css" media="screen" title="no title" charset="utf-8">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="/js/head.js" charset="utf-8"></script>
		<link rel="stylesheet" href="/css/footer.css" media="screen" title="no title" charset="utf-8">
		<!--Bootstrap-->
		<link rel="stylesheet" href="/plugin/bootstrap/css/bootstrap.min.css">
		<script src="/plugin/bootstrap/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap.material-design/0.3.0/css/material-wfont.min.css">
		<link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap.material-design/0.3.0/css/material.min.css">
		<link rel="stylesheet" href="//cdn.jsdelivr.net/bootstrap.material-design/0.3.0/css/ripples.min.css">
		<script src="//cdn.jsdelivr.net/bootstrap.material-design/0.3.0/js/material.min.js"></script>
		<script src="//cdn.jsdelivr.net/bootstrap.material-design/0.3.0/js/ripples.min.js"></script>
	<!--Flash-->
	<link href="/css/flash.css" rel="stylesheet" />
	<script src="/js/pace.min.js"></script>
</head>
<body>
	<header>
	<!--Menu-->
		<div class="contenedor">
			<div class="menu_bar">
			<a href="#" class="bt-menu"><span class="icon-reorder"></span>Menu</a>
		</div>
		<nav >
			<ul>
				<li><a href="/index.php"><span class="icon-home2"></span>Inicio</a></li>
				<li><a href="/blog.php"><span class="icon-newspaper"></span>Blog</a></li>
				<li><a href="/cursos.php"><span class="icon-youtube2" ></span>Cursos</a></li>
				<li><a href="/apps.php"><span class="icon-phonelink"></span>Mis aplicaciones</a></li>
			</ul>
		</nav>
		</div>
	</header>
